use std::hash::Hash;
use std::{
    collections::{HashMap, HashSet},
    fmt::{Display, Formatter},
    fs::File,
    io::{BufRead, BufReader},
    path::Path,
};

#[derive(Debug)]
/// Manis rakstītā koda izsaukto kļūdu pārskaitījums
pub enum AutomataError {
    /// Kāds parametrs vispār nav norādīts
    ParameterNotSpecified(String),
    /// Vispārēja automāta definēšanas kļūda
    DefinitionError(String),
    /// Norādītais akceptējošais stāvoklis nav atrasts
    AcceptedStateNotFound,
    /// Norādītā pāreja ir nekorekta
    InvalidTransition(String),
    /// Pārbaudāmais vārds satur nekorektu simbolu
    InvalidCharacter(char),
}

impl std::error::Error for AutomataError {}

/// Informācija, kā manis veidotās kļūdas attēlot kā lietotājam rādāmu tekstu
impl Display for AutomataError {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        match self {
            AutomataError::ParameterNotSpecified(e) => {
                write!(f, "Automātam trūkst parametrs: {}", e)
            }
            AutomataError::DefinitionError(e) => write!(f, "Automāta definēšanas kļūda: {}", e),
            AutomataError::AcceptedStateNotFound => {
                write!(f, "Akceptējošais stāvoklis netika atrasts")
            }
            AutomataError::InvalidTransition(transition) => {
                write!(f, "Aprakstītā pāreja \"{}\" ir nekorekta", transition)
            }
            AutomataError::InvalidCharacter(symbol) => {
                write!(
                    f,
                    "Ievadītais vārds satur simbolu \"{}\", kas neatrodas alfabētā",
                    symbol
                )
            }
        }
    }
}

/// Automātu aprakstošais objekts/struktūra
#[derive(Debug)]
pub struct Automata {
    /// Stāvokļu vektors
    states: Vec<AutomataState>,
    /// Pāreju vektors
    transitions: Vec<Transition>,
    /// Alfabēta simbolu kopa
    alphabet: HashSet<char>,
    /// Sākuma stāvokļa indekss
    start: usize,
}

impl Automata {
    fn new(
        states: Vec<AutomataState>,
        transitions: Vec<Transition>,
        alphabet: HashSet<char>,
        start: usize,
    ) -> Self {
        let mut automata = Automata {
            states,
            transitions,
            alphabet,
            start,
        };
        automata.initialise_transitive_closures();
        return automata;
    }

    /// Funkcija teksta rindiņas pārveidošanai par alfabēta simbolu kopu
    fn alphabet_from_line(alphabet_line: &str) -> Result<HashSet<char>, AutomataError> {
        let mut symbols = HashSet::new();
        // Rindiņa tiek sadalīta fragmentos, kurus atdala "whitespace".
        // Tas gan nozīmē, ka nav iespējams norādīt atstarpi kā derīgu alfabēta simbolu,
        // bet mēģinājumā šādu scenāriju atbalstīt ir, manuprāt, pārāk lielas iespējas ielaist kļūdas,
        // ja atstarpes izmantojam arī kā vērtību atdalītājus.
        for symbol in alphabet_line.split_whitespace() {
            let chars: Vec<char> = symbol.chars().collect();
            match chars.len() {
                // Simbolu pievieno alfabētam tikai tad, ja ielasītais fragments ir garumā 1
                1 => {
                    // HashSet::insert() atgriež false, ja kopa jau saturēja padoto vērtību
                    if !symbols.insert(chars[0]) {
                        return Err(AutomataError::DefinitionError(format!(
                            "Alfabētā simbols \"{}\" definēts vairākas reizes!",
                            chars[0]
                        )));
                    }
                }
                2.. => {
                    return Err(AutomataError::DefinitionError(format!(
                        "Alfabētā simbols \"{}\" sastāv no vairāk nekā viena burta!",
                        symbol
                    )));
                }
                _ => {
                    return Err(AutomataError::DefinitionError(
                        "Kļūda alfabēta ielasīšanā".to_string(),
                    ));
                }
            }
        }

        Ok(symbols)
    }

    /// Vārda pārbaudīšanas funkcija - vai vārds pieder avota aprakstītajai valodai
    pub fn check_word(&self, word: &str) -> Result<bool, AutomataError> {
        // Atlasa sākuma stāvokli
        let mut current_states = HashSet::from([self.start]);

        // Atrod sākuma stāvokļa transitīvo slēgumu
        current_states = self.find_transitive_closure(&current_states);

        // Iterē cauri vārda simboliem
        for symbol in word.chars() {
            // Kļūda, ja padotais vārds satur simbolus, kuru nav alfabētā
            if !self.alphabet.contains(&symbol) {
                return Err(AutomataError::InvalidCharacter(symbol));
            }

            // Atrod stāvokļus, kuros nonāk "tieši" - bez eps pārejām
            current_states =
                Automata::find_next_states(&self.transitions, &current_states, Some(symbol));

            // Iegūst jauno stāvokļu TS
            current_states = self.find_transitive_closure(&current_states);

            // Ja kādā brīdī stāvokļu kopa ir tukša, no tā vairs nevar atkopties un avots neakceptē vārdu
            if current_states.is_empty() {
                return Ok(false);
            }
        }

        // Atgriež vai galā iegūtajā stāvokļu kopā akceptējošo stāvokļu apakškopa nav tukša
        Ok(current_states.into_iter().any(|x| self.states[x].accepted))
    }

    /// Katram stāvoklim uzstāda tā transitīvo slēgumu
    fn initialise_transitive_closures(&mut self) {
        for (index, automata_state) in self.states.iter_mut().enumerate() {
            // unvisited_states būs to stāvokļu kopa, kuros automāts atrodas, bet no kuriem vēl nav iets pa eps pārejām
            let mut unvisited_states = HashSet::from([index]);

            // states būs tie stāvokļi, kuri jau ir apmeklēti
            let mut states = HashSet::new();
            while !unvisited_states.is_empty() {
                // eps_states = stāvokļu kopa, kuros var nonākt no kāda no unvisited_states stāvokļiem pa eps pāreju vienā solī
                let eps_states =
                    Automata::find_next_states(&self.transitions, &unvisited_states, None);

                // unvisited_states tagad ir apmeklēti, tādēļ tiek pievienoti pie states
                states.extend(unvisited_states.iter());

                // unvisited_states kļūst par eps_states elementu apakškopu, kas satur stāvokļus, kuri vēl nav apmeklēti
                // (paņem kopu starpību `eps_states - states`)
                unvisited_states = HashSet::new();
                for state in eps_states.difference(&states) {
                    unvisited_states.insert(*state);
                }

                // Turpina pildīt, līdz vairs netiek atrasti jauni stāvokļi
            }
            // saglabā stāvokļa transitīvo slēgumu
            automata_state.transitive_closure = states;
        }
    }

    /// Pārveido `states` par `TS(states)`
    fn find_transitive_closure(&self, states: &HashSet<usize>) -> HashSet<usize> {
        let mut transitive_closure = HashSet::new();
        for &index in states {
            transitive_closure.extend(self.states[index].transitive_closure.iter());
        }
        return transitive_closure;
    }

    /// Atgriež stāvokļu kopu, kas atbilst stāvokļiem, kuros var vienā solī nokļūt no kāda no `current_states` stāvokļiem, ielasot simbolu `input_symbol`
    ///
    /// Ja `input_symbol` ir None, tas tiek uzskatīts par epsilonu.
    fn find_next_states(
        transitions: &Vec<Transition>,
        current_states: &HashSet<usize>,
        input_symbol: Option<char>,
    ) -> HashSet<usize> {
        // Atrod "aktīvās" pārejas (tikai tās, kuras tiek "iedarbinātas" saistībā ar simbola ielasīšanu)
        let mut activated_transitions = HashSet::<&Transition>::new();
        // Lai to izdarītu, iterē cauri šībrīža stāvokļiem
        for &state_index in current_states {
            let transitions_for_this_char: HashSet<&Transition> = transitions
                .iter()
                .filter(|transition| {
                    // Atrod tādas pārejas, kuras iziet no sākuma stāvokļa un izmanto ielasīto simbolu.
                    transition.from == state_index && transition.symbol == input_symbol
                })
                .collect();

            // Aktīvo stāvokļu kopai pieskaita tikko izfiltrētos stāvokļus
            activated_transitions.extend(transitions_for_this_char.into_iter());
        }

        // No aktīvajām pārejām izgūst jauno stāvokļu kopu
        activated_transitions
            .iter()
            .map(|transition| transition.to)
            .collect()
    }
}

/// Automāta stāvokli aprakstoša struktūra
#[derive(Debug)]
struct AutomataState {
    /// Stāvokļa nosaukums
    name: String,
    /// Vai stāvoklis ir akceptējošs
    accepted: bool,
    /// Stāvokļa transitīvais slēgums
    transitive_closure: HashSet<usize>,
}

impl AutomataState {
    /// Jauna stāvokļa izveide
    pub fn new(name: String) -> Self {
        AutomataState {
            name,
            accepted: false,
            transitive_closure: HashSet::new(),
        }
    }

    /// Stāvokļu izveide no teksta rindiņas
    fn from_line(state_line: &str) -> Result<Vec<AutomataState>, AutomataError> {
        // `states` ir automāta stāvokļu masīvs
        let mut states = Vec::new();
        let mut taken_names = HashSet::<&str>::new();
        // Teksta rindiņa tiek sadalīta fragmentos, kurus atdala "whitespace"
        for name in state_line.split_whitespace() {
            // pārbauda, vai stāvokļa nosaukums jau nav aizņemts
            match taken_names.insert(name) {
                true => states.push(AutomataState::new(name.to_string())),
                false => {
                    return Err(AutomataError::DefinitionError(format!(
                        "Norādīti divi stāvokļi ar vienādiem nosaukumiem: \"{}\"",
                        name
                    )))
                }
            }
        }

        // Veiksmīgā scenārijā atgriež automāta stāvokļu vektoru
        Ok(states)
    }

    /// Uzstāda akceptējošos stāvokļus
    /// `accepted_line` - teksta rindiņa, kas satur informāciju par akceptējošo stāvokli
    /// `states` - automāta stāvokļu un to nosaukumu Map
    fn set_accepted_states(
        accepted_line: &str,
        states: &mut Vec<AutomataState>,
        state_lookup: &HashMap<String, usize>,
    ) -> Result<(), AutomataError> {
        // Teksta rindiņu sadala fragmentos pa "whitespace", apvieno akceptējamo stāvokļu nosaukumu vektorā
        for accepted_state_name in accepted_line.split_whitespace() {
            match state_lookup.get(accepted_state_name) {
                Some(&index) => states[index].accepted = true,
                None => return Err(AutomataError::AcceptedStateNotFound),
            }
        }
        Ok(())
    }
}

/// Pāreju aprakstoša struktūra
#[derive(Debug, Eq, PartialEq, Hash)]
struct Transition {
    /// Indekss stāvoklim, no kura pāreja iziet
    from: usize,
    /// Indekss stāvoklim, kurā pāreja ieiet
    to: usize,
    /// Pārejas simbols; `None` apzīmē epsilonu.
    symbol: Option<char>,
}

impl Transition {
    /// Vienas pārejas izveide no teksta rindiņas
    fn from_line(
        line: &str,
        state_lookup: &HashMap<String, usize>,
        alphabet: &HashSet<char>,
    ) -> Result<Self, AutomataError> {
        // Teksta rindiņa tiek sadalīta fragmentos, kurus atdala "whitespace"
        let parts: Vec<&str> = line.split_whitespace().collect();
        match parts.len() {
            // Ja rindiņā atrodas divi fragmenti, tad aprakstīta epsilona pāreja
            2 => match (state_lookup.get(parts[0]), state_lookup.get(parts[1])) {
                // Pārbauda, ka ir atrodami abiem nosaukumiem atbilstošie stāvokļi
                (Some(&from), Some(&to)) => {
                    return Ok(Transition {
                        from,
                        to,
                        symbol: None,
                    });
                }
                // Ja nav atrodami atbilstošie stāvokļi, atgriež kļūdu
                _ => return Err(AutomataError::InvalidTransition(line.to_string())),
            },
            // Ja rindiņā atrodas trīs fragmenti, tad aprakstīta parastā pāreja
            3 => {
                // Pārbauda, vai kā simbols ir norādīts tiešām viens simbols, nevis virkne, un to saglabā iekš `input_char`
                let input_chars: Vec<char> = parts[1].chars().collect();
                let input_char = match input_chars.len() {
                    1 => input_chars
                        .first()
                        .expect("already checked, that this is a single char"),
                    _ => {
                        return Err(AutomataError::DefinitionError(
                            "Pārejā kā simbols norādīta vairāku simbolu virkne!".to_string(),
                        ))
                    }
                };

                match (
                    state_lookup.get(parts[0]),
                    state_lookup.get(parts[2]),
                    alphabet.get(&input_char),
                ) {
                    // Pārbauda, ka ir atrodami atbilstošie stāvokļi un ka pārejas simbols ir atrodams alfabētā
                    (Some(&from), Some(&to), Some(symbol)) => {
                        // Izveido jaunu pārejas struktūru ar ielasītajiem parametriem
                        return Ok(Transition {
                            from,
                            to,
                            symbol: Some(*symbol),
                        });
                    }
                    // Pretējā gadījumā atgriež kļūdu
                    _ => return Err(AutomataError::InvalidTransition(line.to_string())),
                }
            }
            // Ja rindiņā ir jebkāds cits skaits fragmentu, notikusi kļūda
            _ => return Err(AutomataError::InvalidTransition(line.to_string())),
        }
    }
}

/// Automāta objekta izveide no apraksta failā
pub fn parse_input_file(file_path: &Path) -> Result<Automata, Box<dyn std::error::Error>> {
    // TODO: separate reading from the file and initializing the Automata into separate functions
    // Atver failu
    let file = File::open(file_path)?;
    let reader = BufReader::new(file);

    // Saturu sadala pa rindiņām un pieglabā iteratoru mainīgajā
    let mut lines = reader.lines();

    let mut states;
    let alphabet;
    let start;

    // Pirmā faila rinda satur stāvokļu aprakstu
    match lines.next() {
        // Ja rindiņa nav tukša, izsauc stāvokļu izveides funkciju
        Some(state_line) => states = AutomataState::from_line(&state_line?)?,
        None => {
            return Err(Box::new(AutomataError::ParameterNotSpecified(
                "nav norādīts neviens stāvoklis!".to_string(),
            )))
        }
    }

    // Izveido stāvokļu lookup
    let mut state_lookup = HashMap::new();
    for (index, state) in states.iter().enumerate() {
        state_lookup.insert(state.name.to_string(), index);
    }

    // Otrā faila rindiņa satur alfabētu
    match lines.next() {
        // Ja rindiņa nav tukša, izsauc alfabēta izveides funkciju
        Some(alphabet_line) => alphabet = Automata::alphabet_from_line(&alphabet_line?)?,
        None => {
            return Err(Box::new(AutomataError::ParameterNotSpecified(
                "alfabētā nav norādīts neviens simbols!".to_string(),
            )))
        }
    }

    // Trešā faila rindiņa satur sākuma stāvokli
    match lines.next() {
        // Ja rindiņa nav tukša un ir atrodams stāvoklis ar atbilstošo nosaukumu,
        // saglabā ievadīto sākuma stāvokļa nosaukumu.
        // Pretējos gadījumos atgriež atbilstošās kļūdas.
        Some(starting_line) => {
            let starting_line = starting_line?;
            match state_lookup.get(&starting_line) {
                Some(&index) => start = index,
                _ => {
                    return Err(Box::new(AutomataError::DefinitionError(
                        "sākuma stāvoklis netika atrasts starp definētajiem stāvokļiem!"
                            .to_string(),
                    )))
                }
            }
        }
        None => {
            return Err(Box::new(AutomataError::ParameterNotSpecified(
                "nav norādīts sākuma stāvoklis".to_string(),
            )))
        }
    }

    // Ceturtā faila rindiņa satur akceptējošos stāvokļus
    match lines.next() {
        // Ja rindiņa nav tukša, izsauc funkciju, kas izmaina stāvokļu struktūras, atbilstoši uzstādot parametru `accepted`
        Some(accepted_line) => AutomataState::set_accepted_states(&accepted_line?, &mut states, &state_lookup)?,
        None => {
            return Err(Box::new(AutomataError::ParameterNotSpecified(
                "nav norādīts akceptējošais stāvoklis (atbilde jebkurā gadījumā - vārds nepieder valodai)".to_string(),
            )))
        }
    }

    // Visas pārējās rindiņas satur pāreju aprakstus
    let mut transitions = Vec::new();
    // Iterē cauri rindām, katrai izsauc pārejas izveides funkciju un pievieno rezultātu pāreju vektoram.
    for line in lines {
        transitions.push(Transition::from_line(&line?, &state_lookup, &alphabet)?)
    }

    // Atgriež automātu atbilstoši ielasītajai informācijai
    Ok(Automata::new(states, transitions, alphabet, start))
}
