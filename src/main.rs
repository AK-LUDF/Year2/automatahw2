use std::{io::stdin, path::Path};

/// Konstante - faila nosaukums, kurā definēts automāts
///
/// Fails jānovieto programmas root mapītē pie `Cargo.toml`, nevis `src` apakšmapē.
const FILE_NAME: &str = "avots.txt";

fn main() {
    let automata;

    // Iegūst automāta un saistītos objektus/struktūras, ielasot no failu
    match automata_simulator::parse_input_file(Path::new(FILE_NAME)) {
        Ok(new_automata) => automata = new_automata,
        // Kļūdas gadījumā to izdrukā un beidz darbu
        Err(e) => panic!("{}", e),
    }

    // Cikliski ievada un pārbauda vārdu, lai katra vārda pārbaudīšanai nebūtu vēlreiz jāpalaiž programma
    loop {
        println!("Ievadiet vārdu:");

        let mut buffer = String::new();
        if let Err(e) = stdin().read_line(&mut buffer) {
            panic!("Kļūda, ielasot no stdin: {}", e);
        }

        // Tiek nogriezts ievades beigās esošs "whitespace".
        // Tas nepieciešams, jo pēc ielasīšanas no stdin, bufera beigās atrodas \n vai \r\n simboli.
        let input_text = String::from(buffer.trim_end());

        // Izsauc vārda pārbaudīšanu ar ievadīto tekstu, kļūdas gadījumā to izdrukā un pāriet uz nākamo iterāciju (vēlreiz prasa vārdu).
        let result_text;
        match automata.check_word(&input_text) {
            Ok(res) => {
                if res {
                    result_text = "pieder";
                } else {
                    result_text = "nepieder";
                }
            }
            Err(e) => {
                eprintln!("Kļūda: {}", e);
                continue;
            }
        }

        println!(
            "Vārds \"{}\" {} valodai, kuru apraksta avots failā \"{}\".",
            input_text, result_text, FILE_NAME
        );
    }
}
