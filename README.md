# Avota interpretators

Autors: Andrejs Ķīlis ak20157

## Kā izmantot
Nepieciešams uzinstalēt standarta Rust programmēšanas valodas rīkus (konkrēti `cargo`), visvieglāk to izdarīt ar rustup. [Instrukcijas šeit](https://www.rust-lang.org/learn/get-started).

Lai programmu palaistu, pēc rīku instalācijas komandrindā jāatver mape, kurā atrodas šis fails un jāpalaiž komanda `cargo run`.

Pēc noklusējuma programma avotu ielasa no faila `avots.txt`, kas atrodas šajā pašā mapē. To var viegli nomainīt ar konstanti `FILE_NAME` faila `src/main.rs` augšpusē.
